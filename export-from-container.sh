#!/bin/bash

set -x

emacs --batch -load emacs.el --file "$1" -f org-latex-export-to-pdf >& log
cat log