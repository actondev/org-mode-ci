#!/bin/bash

set -x
docker run --rm -i -t -v $(pwd):$(pwd) --workdir=$(pwd) actondev/org-mode-docker:latest emacs --batch "$1" -f org-latex-export-to-pdf >& log

cat log
